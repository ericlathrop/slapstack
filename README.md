# Slapstack

## Deploy parameterized AWS CloudFormation Stacks

Slapstack deploys [AWS CloudFormation](https://aws.amazon.com/cloudformation/) templates quickly, safely, and with
multiple configurations.

Quickly: a simple command line makes it easy to remember the command:

  `npx slapstack stack.yml commit`

Safely: plugins ensure all your work is committed, your Docker images are
pushed, and Slack is notified.

Multiple configurations: "stack files" provide different parameters to your
CloudFormation templates, and can import common settings.

## Requirements

* [Node.js](https://nodejs.org/)
* [aws-cli](https://aws.amazon.com/cli/)

## Installation

* `npm install --save-dev slapstack`

## Stack Files

Slapstack uses a configuration file called a "stack file" to provide the
necessary parameters to deploy a CloudFormation template. Stack files can be
written in either YAML or JSON. In this documentation we'll use YAML.

### Example

Let's say you have a CloudFormation template to deploy an S3 bucket named
`data-bucket.yml` with the following contents:

```yaml
Parameters:
  EnvironmentParameter:
    Type: String
Resources:
  DataBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub "data-bucket-${EnvironmentParameter}"
```

If you wanted to deploy the same CloudFormation templates to two different
environments, you might create a `data-bucket-development.yml` stack file with
these contents:

```yaml
parameters:
  EnvironmentParameter: development
stackName: data-bucket-development
template: data-bucket.yml
```

and a `data-bucket-production.yml` stack file with these contents:

```yaml
parameters:
  EnvironmentParameter: production
stackName: data-bucket-production
template: data-bucket.yml
```

With these stack files you could run `npx slapstack data-bucket-development.yml
commit` to deploy the development version of your S3 bucket.

### `stackName`

The `stackName` key holds a string value that is the name of this stack in
CloudFormation.

### `stackPolicy`

The `stackPolicy` key holds a string which is a path of a file containing CloudFormation stack
policy file. Stack policies are a good way to [prevent a stack update from
accidentally deleting your database](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/protect-stack-resources.html).

### `template`

The `template` key holds a string value that is the path name to the
CloudFormation template file for this stack. The path is relative to the stack
file.

### `parameters`

The `parameters` key holds a key/value object of any parameters you want to pass
to your CloudFormation template.

### `imports`

The `imports` key holds an array of string values that is a list of paths of
files to merge into the current stack file before deployment. You could use this
to store common settings for each environment, or to store secrets in a separate
file that's ignored from version control.

Let's extract the production environment parameters into a `production.yml` file
with these contents:

```yaml
parameters:
  EnvironmentParameter: production
```

Then we can modify the `data-bucket-production.yaml` stack file to import our
new file like this:

```yaml
imports:
  - production.yml
stackName: data-bucket-production
template: data-bucket.yml
```

### `awsCliProfile`

The `awsCliProfile` key holds a string value that is the aws-cli profile name
to use when deploying this stack file. The default is "default". This allows you
to use different AWS credentials and accounts for each stack.

### `awsEcr`

The `awsEcr` key holds a key/value object that holds information for checking
that docker images are pushed to [AWS ECR](https://aws.amazon.com/ecr/).

Specifying `awsEcr` will cause Slapstack to ensure that a tag named the same
as the `commit` from the slapstack invocation exists in the named repository. If
the tag does not exist, the deployment will fail.

```yaml
awsEcr:
  parameter: ImageParameter
  profile: my_aws_cli_profile_name
  region: us-east-1
  repository: 000000000000.dkr.ecr.us-east-1.amazonaws.com/organization/repository
```

### `capabilities`

The `capabilities` key holds a list of capabilities to send to CloudFormation
when processing your template. [See the CreateStack
documentation](https://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/API_CreateStack.html).
A common example would be including CAPABILITY_IAM when your template contains
an IAM resource.

### `dockerHub`

The `dockerHub` key holds a key/value object that holds information for checking
that docker images are pushed to [Docker Hub](https://hub.docker.com/).

Specifying `dockerHub` will cause Slapstack to ensure that a tag named the same
as the `commit` from the slapstack invocation exists in the named repository. If
the tag does not exist, the deployment will fail.

```yaml
dockerHub:
  repository: organization/repository
  username: username
  password: password
  parameter: ImageParameter
```

The `repository` sub-key represents a repository on Docker Hub and should be in the form of
`organization/repository`. The `username` sub-key is the name of a Docker Hub
user. The `password` sub-key is the password for the user named in `username`.
The `parameter` sub-key is the name of a parameter that will be passed to your
CloudFormation template containing the full name of the docker image to deploy
in the form of `organization/repository:tag`.

### `lambda`

The `lambda` key holds a key/value object of settings for uploading a code
package for [AWS Lambda](https://aws.amazon.com/lambda/).

```yaml
lambda:
  folder: lambdas
  bucket: my-lambda-bucket
  parameter: CodeUriKeyParameter
```

The `folder` sub-key is a path to a folder to zip and upload to S3. The `bucket`
sub-key is the name of a bucket where the zip file should be uploaded. The
`parameter` sub-key is the name of a parameter that will be passed to your
CloudFormation template containing the URI to the zip file on S3.

### `slack`

The `slack` key holds a key/value object that holds information for sending a
message to a Slack channel when a deployment is started.

```yaml
slack:
  webhookUrl: https://hooks.slack.com/services/...
  channel: my-channel
```
