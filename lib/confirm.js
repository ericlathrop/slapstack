const readline = require("readline");

module.exports = function confirm(question) {
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  return new Promise(resolve => {
    rl.question(question, function(answer) {
      var confirmed = answer.match(/^y(es)?$/i);
      rl.close();
      resolve(confirmed);
    });
  });
};
