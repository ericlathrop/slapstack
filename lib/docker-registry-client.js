const fetch = require("node-fetch");

function getManifest(registry_host, repository, tag, basic_auth) {
  var url = `https://${registry_host}/v2/${repository}/manifests/${tag}`;

  return fetch(url, {
    headers: new fetch.Headers({
      "Authorization": `Basic ${basic_auth}`
    })
  }).then(res => checkStatus(res, url)).then(res => res.json());
}

function checkStatus(res, url) {
  if (res.ok) {
    return res;
  } else {
    throw `Unable to list docker tags at ${url}: ${res.status} ${res.statusText}`;
  }
}

module.exports = {
  getManifest: getManifest
};
