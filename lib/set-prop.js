module.exports = (obj, prop) => value => {
  obj[prop] = value;
  return obj;
};
