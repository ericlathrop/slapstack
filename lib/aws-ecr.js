const exec = require("./exec");
const drc = require("./docker-registry-client");

function getManifest(profile, region, repository, tag) {
  var token = exec.forStdout(
    "aws", "ecr", "get-authorization-token",
    "--profile", profile,
    "--region", region,
    "--output", "text",
    "--query", "authorizationData[].authorizationToken"
  ).trim();

  var url = new URL(`https://${repository}`);
  return drc.getManifest(url.hostname, url.pathname.substr(1), tag, token);
}

function ensureTagIsPushed(profile, region, repository, tag) {
  return getManifest(profile, region, repository, tag).catch(() => {
    var name = repository + ":" + tag;
    throw "Unable to find " + name + " on AWS ECR. Please run `docker push " + name + "`.";
  });
}

module.exports = {
  ensureTagIsPushed: ensureTagIsPushed
};
