const awsEcr = require("../aws-ecr");

function detectDockerImage(args) {
  if (!args.stack.awsEcr || !args.stack.awsEcr.repository) {
    return args;
  }

  var dockerRepo = args.stack.awsEcr.repository;

  args.stack.parameters[args.stack.awsEcr.parameter] = dockerRepo + ":" + args.commit;

  return awsEcr.ensureTagIsPushed(
    args.stack.awsEcr.profile,
    args.stack.awsEcr.region,
    dockerRepo,
    args.commit
  ).then(() => args);
}

module.exports = {
  configuration: detectDockerImage
};
