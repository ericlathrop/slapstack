const commandExists = require("command-exists");

function ensureAwsCliIsInstalled(args) {
  return commandExists("aws")
    .then(() => args)
    .catch(() => {
      throw "Command `aws` does not exist. Please ensure the aws-cli package is installed. https://aws.amazon.com/cli/";
    });
}

module.exports = {
  configuration: ensureAwsCliIsInstalled
};
