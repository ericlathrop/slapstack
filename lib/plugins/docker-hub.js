const dockerHub = require("../docker-hub");

function detectDockerImage(args) {
  if (!args.stack.dockerHub || !args.stack.dockerHub.repository) {
    return args;
  }

  var dockerRepo = args.stack.dockerHub.repository;

  args.stack.parameters[args.stack.dockerHub.parameter] = dockerRepo + ":" + args.commit;

  return dockerHub.ensureTagIsPushed(
    dockerRepo,
    args.commit
  ).then(() => args);
}

module.exports = {
  configuration: detectDockerImage
};
