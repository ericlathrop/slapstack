#!/usr/bin/env node

const confirm = require("./lib/confirm");
const deepmerge = require("deepmerge");
const exec = require("./lib/exec");
const path = require("path");
const util = require("util");
const yaml = require("js-yaml");

const fs = require("fs");
const readFile = util.promisify(fs.readFile);

function parseArgs(args) {
  var stackFile = args[0];
  var commit = args[1];
  if (stackFile === undefined || commit === undefined) {
    console.error("Usage: npx slapstack stack.yml commit");
    process.exit(1);
  }

  var stackPath = path.resolve(process.cwd(), stackFile);

  return Promise.resolve({
    commit: commit,
    plugins: [
      require("./lib/plugins/git"),
      require("./lib/plugins/aws"),
      require("./lib/plugins/aws-ecr"),
      require("./lib/plugins/docker-hub"),
      require("./lib/plugins/lambda"),
      require("./lib/plugins/slack")
    ],
    stackPath: stackPath
  });
}

const defaultStack = {
  awsCliProfile: "default",
  capabilities: []
};

var executePluginEvent = event => args => {
  return args.plugins.reduce((promise, plugin) => {
    return promise.then(pluginArgs => {
      if (typeof plugin[event] === "function") {
        return plugin[event](pluginArgs);
      } else {
        return pluginArgs;
      }
    });
  }, Promise.resolve(args));
};

function loadStack(args) {
  return readFile(args.stackPath, "utf8")
    .then(data => parseByType(args.stackPath, data))
    .then(stack => {
      var importList = stack.imports || [];
      var imports = importList.map(importPath => loadImport(args.stackPath, importPath));
      return Promise.all(imports)
        .then(importDatas => {
          var allStacks = [defaultStack].concat(importDatas, stack);
          args.stack = deepmerge.all(allStacks);
          return args;
        });
    }).then(loadStackPolicy);
}

function loadImport(basePath, targetPath) {
  var fullPath = path.resolve(path.dirname(basePath), targetPath);
  return readFile(fullPath)
    .then(data => parseByType(fullPath, data));
}

function parseByType(filename, data) {
  if (filename.endsWith(".yaml") || filename.endsWith(".yml")) {
    return yaml.load(data);
  } else {
    return JSON.parse(data);
  }
}

function loadStackPolicy(args) {
  if (typeof args.stack.stackPolicy !== "string" || args.stack.stackPolicy.length === 0) {
    return args;
  }

  var fullPath = path.resolve(path.dirname(args.stackPath), args.stack.stackPolicy);
  return readFile(fullPath).then(policy => {
    args.stack.stackPolicyBody = policy.toString();
    return args;
  });
}

function confirmDeploy(args) {
  var message = `
  Stack:       ${args.stack.stackName}
  Profile:     ${args.stack.awsCliProfile}

Are you sure? `;
  return confirm(message)
    .then(confirmed => {
      if (!confirmed) {
        process.exit(3);
      }
      return args;
    });
}

function deploySite(args) {
  console.log("Deploying", args.stack.stackName, "...");

  var command = ["aws", "cloudformation", "deploy",
    "--profile", args.stack.awsCliProfile,
  ];

  if (args.stack.capabilities.length > 0) {
    command = command.concat(["--capabilities"], args.stack.capabilities);
  }

  command = command.concat([
    "--stack-name", args.stack.stackName,
    "--template-file", path.resolve(path.dirname(args.stackPath), args.stack.template),
    "--parameter-overrides",
    "CommitParameter=" + args.commit,
  ]);

  var parameters = Object.keys(args.stack.parameters || {}).reduce((acc, key) => {
    acc.push(key + "=" + args.stack.parameters[key].toString());
    return acc;
  }, []);
  command = command.concat(parameters);

  args.exitCode = exec.forExitCode.apply(this, command);
  return args;
}

function setStackPolicy(args) {
  if (typeof args.stack.stackPolicyBody !== "string" || args.stack.stackPolicyBody.length === 0) {
    return args;
  }

  console.log("Setting", args.stack.stackName, "stack policy ...");

  var command = ["aws", "cloudformation", "set-stack-policy",
    "--profile", args.stack.awsCliProfile,
    "--stack-name", args.stack.stackName,
    "--stack-policy-body", args.stack.stackPolicyBody
  ];

  exec.forExitCode.apply(this, command);
  return args;
}

parseArgs(process.argv.slice(2))
  .then(loadStack)
  .then(executePluginEvent("configuration"))
  .then(confirmDeploy)
  .then(executePluginEvent("beforeDeploy"))
  .then(deploySite)
  .then(executePluginEvent("afterDeploy"))
  .then(setStackPolicy)
  .then(executePluginEvent("afterSetStackPolicy"))
  .catch(err => {
    console.error(err);
    process.exit(4);
  });
